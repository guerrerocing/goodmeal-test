
# GoodMeal Test

Proyecto para postular a puesto de trabajo en GoodMeal


## Tech Stack

**Cliente:** Vue, Vue-Apollo, Vue-Router, ElementPlus

**Servidor:** PHP, Laravel, PHP-LightHouse(GraphQL)


## Ejecutar Localmente

Clonar proyecto

```bash
  git clone https://gitlab.com/guerrerocing/goodmeal-test.git
```

Ir al directorio del Proyecto

```bash
  cd goodmeal-test
```

Instalar dependencias

```bash
  yarn install
```



Asegurate de no tener instancias de Postgres, PHP o NGINX corriendo.

creamos nuestro archivo .env

```bash
  cp .env.example .env
```
Ejecutar comandos de Docker
```bash
  docker-compose up --build -d
```
Accedemos a nuestro contenedor
```bash
  docker exec -it Laravel_php /bin/sh
```


## Instalacion de dependencias


```bash
  composer install

  php artisan key:generate

  php artisan migrate
  
  composer ide-helper
```

## Ejecutar FrontEnd

Nos Aseguramos de estar fuera de nuestro contenedor pero dentro de nuestro proyecto y ejecutamos:

```bash
yarn dev
```

Ahora podemos acceder a nuestro proyecto en nuestro navegador
```
http://localhost:8000
```
## API Reference

#### API GraphQL

Aca se encuentra la documentacion de la API con todas sus queries
y mutaciones.

```http
  http://localhost:8000/graphiql
```

#### GraphQL Endpoint

```http
  http://localhost:8000/graphql
```

###Backend Directorios

```
/App/GrahpQL

/graphql

/Models
```

###Vue Directorio

```
/resourses/vue-app
```






# Ejercicio 2


Considera el servicio anteriormente construido, ahora queremos que las tiendas sean visibles
para el usuario usando su geoposicion. Sabemos que podemos consultar un servicio de
terceros, para calcular la distancia matricial (incluyendo calles y paso habilitados) desde la
tienda hasta donde está el usuario, para así mostrarle las tiendas de manera que le aparezcan
siempre las más cercanas e indicarle al usuario la distancia que existe.
El problema es que el servicio es caro, por lo tanto, debemos optimizar su consumo.
- Como se te ocurre un modelo para almacenar la distancia de tiendas?
```
Considero que seria necesario almacenar la ubicacion de la tienda 
(longitud, latitude) y el radio en kilometros para poder realizar calculos en base 
a la ubicacion del cliente.

El radio nos permitira saber en que rango las tiendas 
pueden enviar pedidos a sus clientes. Con esto si esta fuera de rango 
no le presentariamos la tienda al usuario o le mostrariamos la tienda mas cercana 
en caso de que esta tienda cuente con muchas sucursales
```
- Como podemos hacer que el calculo de distancia de tiendas, sea rapido?
```
Para poder realizar estos calculos seria necesario utilizar la extension
de Postgres Postgis que tiene muchas funciones que ayudarian a optimizar 
esos calculos.

Previamente ya he trabajado con esta extension, calculando distancias
para poder mostrar lugares cercanos a una tienda.
```
- Como podemos disminuir el consumo del servicio de terceros ?

- Como podemos optimizar la respuesta de nuestro servicio ?

Respuesta de Ambas preguntas:

```
Considero que siempre hay que pensar en la escalabilidad de las applicaciones
podriamos considerar contar con microservicios que ayuden  mejorar las respuestas
asi tambien hacer uso de cache y mantener una aplicacion modularizada.
contar con una buena arquitectura de software como podria ser event-driven asi tambien
optimizar queries, paginaciones
```



